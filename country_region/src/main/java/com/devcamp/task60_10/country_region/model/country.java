package com.devcamp.task60_10.country_region.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "countries")
public class country {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long country_id;
    @Column(name = "country_code", unique = true)
    private String countryCode;
    @Column(name = "country_name")
    private String countryName;
    
    @OneToMany(mappedBy = "country", cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<region> regions;
    
    public country() {
    }

    public country(String country_Code, String country_Name) {
        this.countryCode = country_Code;
        this.countryName = country_Name;
    }

    public Long getId() {
        return country_id;
    }

    public void setId(Long country_id) {
        this.country_id = country_id;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String country_Code) {
        this.countryCode = country_Code;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String country_Name) {
        this.countryName = country_Name;
    }

    public Set<region> getRegions() {
        return regions;
    }

    public void setRegions(Set<region> regions) {
        this.regions = regions;
    }

    
}
