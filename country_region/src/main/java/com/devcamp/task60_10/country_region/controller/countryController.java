package com.devcamp.task60_10.country_region.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task60_10.country_region.model.country;
import com.devcamp.task60_10.country_region.model.region;
import com.devcamp.task60_10.country_region.repository.countryRepository;
import com.devcamp.task60_10.country_region.repository.regionRepository;


@CrossOrigin
@RestController
@RequestMapping("/")
public class countryController {
    @Autowired
    countryRepository pCountryRepository;

    @Autowired
    regionRepository pRegionRepository;

    @GetMapping("/countries")
    public ResponseEntity<List<country>> getAllCountries(){
        try {
            List<country> pCountries = new ArrayList<country>();

            pCountryRepository.findAll().forEach(pCountries::add);
            
            return new ResponseEntity<>(pCountries, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/regions")
    public ResponseEntity<Set<region>> getRegionByCountryCode(@RequestParam(value = "countryCode") String country_Code){
        try {
            country vCountry = pCountryRepository.findByCountryCode(country_Code);

            if(vCountry != null){
                return new ResponseEntity<>(vCountry.getRegions(), HttpStatus.OK);
            }
            else{
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
