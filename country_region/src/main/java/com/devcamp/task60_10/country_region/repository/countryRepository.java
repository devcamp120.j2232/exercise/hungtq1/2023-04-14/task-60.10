package com.devcamp.task60_10.country_region.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task60_10.country_region.model.country;

public interface countryRepository extends JpaRepository<country, Long> {
    country findByCountryCode(String countryCode);
}
