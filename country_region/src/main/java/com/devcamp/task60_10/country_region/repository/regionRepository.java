package com.devcamp.task60_10.country_region.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task60_10.country_region.model.region;

public interface regionRepository extends JpaRepository<region, Long> {
    
}
